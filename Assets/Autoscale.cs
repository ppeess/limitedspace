using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class Autoscale : MonoBehaviour
{
    CanvasScaler canvasScaler;

    void Start()
    {
        canvasScaler = GetComponent<CanvasScaler>();
    }

    void Update()
    {
        canvasScaler.scaleFactor = Screen.currentResolution.width / 320;
    }
}
