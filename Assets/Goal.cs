using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour
{

    public GameObject error;
    public GameObject victory;

    public void Check()
    {
        if (GameManager.instance.playerManager.playerRating >= 10)
        {
            victory.SetActive(true);
        }
        else
        {
            error.SetActive(true);
        }
    }
}
