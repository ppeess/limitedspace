﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Abilities
{
    [CreateAssetMenu(fileName = "PlayerAbility", menuName = "Abilities/Player Ability")]
    public class PlayerAbility : Ability
    {
        public int charges = 1;
        public PartType target;
    }
}