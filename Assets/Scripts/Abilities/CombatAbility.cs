﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "CombatAbility", menuName = "Abilities/Combat Ability")]
public class CombatAbility : Ability
{
    public int cooldownRounds;
    public CombatTarget target;
    public List<StatModifier> statModifiers;
}