using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : ScriptableObject
{
    public string abilityName;
    public string description;
    public Sprite icon;
    public int cooldown;
    public AbilityType abilityType;
}

public enum AbilityType
{
    Part,
    Player
}