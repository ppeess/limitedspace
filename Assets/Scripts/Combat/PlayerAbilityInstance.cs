﻿using Assets.Scripts.Abilities;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Combat
{
    public class PlayerAbilityInstance
    {
        public PlayerAbility reference;
        public int remainingCharges = 1;
        public int remainingCooldown = 0;

        public PlayerAbilityInstance(PlayerAbility reference) {
            this.reference = reference;
            this.remainingCharges = reference.charges;
        }
    }
}