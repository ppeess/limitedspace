﻿using System;
using System.Collections;
using UnityEngine;

public class CombatManager : MonoBehaviour
{
    public CombatEntity player;
    public CombatEntity enemy;
    public int difficulty = 1;

    public CombatEntity currentEntity;

    public CombatUI combatUI;

    public void Start()
    {
        combatUI = GameManager.instance.uiManager.combatUI;
    }

    public void Init(PlayerManager playerManager, Inventory inventory, EnemyPreset selectedEnemy)
    {
        player = new CombatEntity(playerManager.playerStats, inventory.items, combatUI.playerRobot, true);
        
        selectedEnemy.baseStats.Init();
        difficulty = selectedEnemy.difficulty;
        enemy = new CombatEntity(selectedEnemy.baseStats, selectedEnemy.availableItems, combatUI.enemyRobot);
        
        combatUI.gameObject.SetActive(true);
        combatUI.enemyHealthBar.Init(enemy);
        combatUI.playerHealthBar.Init(player);

        currentEntity = player;
    }

    public void FinishCombat(CombatEntity loser)
    {
        if (loser == player)
        {
            // TODO: Defeat text
            GameManager.instance.uiManager.loseUI.gameObject.SetActive(true);
        }
        else
        {
            // TODO: Win text
            GameManager.instance.uiManager.winUI.gameObject.SetActive(true);
            GameManager.instance.uiManager.winUI.Init(50 * difficulty, difficulty);
            GameManager.instance.inventory.scrap += 50 * difficulty;
            GameManager.instance.playerManager.playerRating += difficulty;
        }
        GameManager.instance.shopData.Reroll();
    }

    public void EndTurn()
    {
        currentEntity.EndRound();
        if (currentEntity == player) 
        {
            combatUI.abilityHolder.SetActive(false);
            currentEntity = enemy;
            DoEnemyTurn();
        }
        else
        {
            currentEntity = player;
            InitializePlayerTurn();
        }
    }

    public void InitializePlayerTurn()
    {
        combatUI.abilityHolder.SetActive(true);
    }

    public void DoEnemyTurn()
    {
        enemy.RerollAll();
        Invoke("EndTurn", 2);
        Invoke("UseAbility", 1);
    }

    public void UseAbility()
    {
        int part = UnityEngine.Random.Range(0, 5);
        bool used = false;
        if (part == 0)
        {
            used = enemy.left.Use();
        }
        else if (part == 1)
        {
            used = enemy.right.Use();
        }
        else if (part == 2)
        {
            used = enemy.cpu.Use();
        }
        else if (part == 3)
        {
            used = enemy.periphery.Use();
        }
        else if (part == 4)
        {
            used = enemy.casing.Use();
        }
        
        if (!used)
        {
            Debug.Log("Did not use an ability");
            // TODO: DO SOMETHING?
        }
    }
}
