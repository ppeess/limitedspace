﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotMachine
{
    public List<Part> Roll(List<Part> parts, int number = 1)
    {
        List<Part> remainingParts = new List<Part>(parts);
        List<Part> selected = new List<Part> ();
        
        for (int i = 0; i < number; i++)
        {
            if (remainingParts.Count > 0)
            {
                Part selectedPart = remainingParts[Random.Range(0, remainingParts.Count)];
                remainingParts.Remove(selectedPart);
                selected.Add(selectedPart);
            }
        }

        return selected;
    }
}
