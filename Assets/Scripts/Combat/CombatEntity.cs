using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CombatEntity
{
    public Stats baseStats;
    public Stats combatStats;
    public RobotUI robotUI;
    public bool isPlayer;

    public List<Item> availableItems;
    public List<Part> availableParts;

    public Part cpu;
    public Part periphery;
    public Part casing;
    public Part left;
    public Part right;

    public CombatEntity(Stats baseStats, List<ItemInstance> availableItems, RobotUI robotUI, bool isPlayer = false) 
    {
        this.robotUI = robotUI;
        this.isPlayer = isPlayer;
        Init(baseStats, availableItems);
    }

    public void Init(Stats baseStats, List<ItemInstance> availableItems)
    {
        this.baseStats = baseStats;
        this.combatStats = new Stats();

        foreach (Stat stat in this.baseStats.stats)
        {
            this.combatStats.stats.Add(new Stat(stat));
        }
        this.combatStats.stats.Add(new Stat("CurrHP", this.baseStats.GetStat("MaxHP")));
        Debug.Log("Just set currHP");

        availableParts = new List<Part>();
        
        foreach (ItemInstance item in availableItems)
        {
            availableParts.Add(new Part(item.item));
        }

        // Select parts randomly using slot machines
        List<Part> possibleCPUs = GetPartsWithType(PartType.CPU);   
        List<Part> possiblePeriphery = GetPartsWithType(PartType.Periphery);
        List<Part> possibleCasings = GetPartsWithType(PartType.Casing);
        List<Part> possibleLeft = GetPartsWithType(PartType.Left);
        List<Part> possibleRight = GetPartsWithType(PartType.Right);

        if (possibleCPUs.Count > 0)
        {
            cpu = new SlotMachine().Roll(possibleCPUs)[0];
            Equip(cpu);
        }
        if (possiblePeriphery.Count > 0)
        {
            periphery = new SlotMachine().Roll(possiblePeriphery)[0];
            Equip(periphery);
        }
        if (possibleCasings.Count > 0)
        {
            casing = new SlotMachine().Roll(possibleCasings)[0];
            Equip(casing);
        }
        if (possibleLeft.Count > 0)
        {
            left = new SlotMachine().Roll(possibleLeft)[0];
            Equip(left);
        }
        if (possibleRight.Count > 0)
        {
            right = new SlotMachine().Roll(possibleRight)[0];
            Equip(right);
        }
    }

    public void Unequip(Part part)
    {
        if (part != null)
        {
            if (!part.disabled)
            {
                foreach(StatModifier statModifier in part.reference.statModifiers)
                {
                    combatStats.ModifyStat(statModifier, -1);
                }
            }
        }
    }

    public void EndRound()
    {
        foreach (Part part in availableParts)
        {
            if (part.remainingCooldown > 0)
            {
                part.remainingCooldown -= 1;
                if (part.remainingCooldown == 0)
                {
                    part.disabled = false;
                }
            }
        }
    }

    public void DisablePart(Part part)
    {
        foreach (StatModifier statModifier in part.reference.statModifiers)
        {
            combatStats.ModifyStat(statModifier, -1);
        }
        GameManager.instance.uiManager.combatUI.UpdateUI();
    }

    public void Equip(Part part)
    {
        if (part != null)
        {
            if (!part.disabled)
            {
                foreach (StatModifier statModifier in part.reference.statModifiers)
                {
                    combatStats.ModifyStat(statModifier, 1);
                }
            }
        }
        GameManager.instance.uiManager.combatUI.UpdateUI();
        robotUI.Init(periphery, cpu, right, left, casing);
    }

    public void Damage(int incomingDamage)
    {
        Debug.Log("Damaged!");
        incomingDamage -= combatStats.GetStat("Def");
        if (incomingDamage > 0)
        {
            combatStats.ModifyStat("CurrHP", incomingDamage, -1);
        }

        if (combatStats.GetStat("CurrHP") <= 0)
        {
            GameManager.instance.combatManager.FinishCombat(this);
        }
    }

    public void RerollAll()
    {
        List<PartType> toReroll = new List<PartType>() { PartType.CPU, PartType.Periphery, PartType.Casing, PartType.Left, PartType.Right};
        foreach (PartType partType in toReroll)
        {
            RerollPart(partType);
        }
    }

    public void RerollPart(PartType partType)
    {
        List<Part> possibleParts = GetPartsWithType(partType);
        switch(partType)
        {
            case PartType.Left:
                Unequip(left);
                left = new SlotMachine().Roll(possibleParts)[0];
                Equip(left);
                break;
            case PartType.Right:
                Unequip(right);
                right = new SlotMachine().Roll(possibleParts)[0];
                Equip(right);
                break;
            case PartType.CPU:
                Unequip(cpu);
                cpu = new SlotMachine().Roll(possibleParts)[0];
                Equip(cpu);
                break;
            case PartType.Periphery:
                Unequip(periphery);
                periphery = new SlotMachine().Roll(possibleParts)[0];
                Equip(periphery);
                break;
            case PartType.Casing:
                Unequip(casing);
                casing = new SlotMachine().Roll(possibleParts)[0];
                Equip(casing);
                break;
            default:
                Debug.LogError("Not allwoed for parttype " + partType.ToString());
                break;
        }
    }

    public List<Part> GetPartsWithType(PartType type)
    {
        List<Part> partList = new List<Part>();
        foreach (Part part in availableParts)
        {
            if (part.reference.partType == type && part.reference.baseType == BaseType.Part)
            {
                partList.Add(part);
            }
        }
        return partList;
    }
}
