using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

[Serializable]
public class Part
{
    public Item reference;
    public int remainingCooldown = 0;
    public bool disabled = false;

    public bool Use()
    {
        if (!disabled && reference.ability != null)
        {
            Debug.Log("Used ability from item " + reference.itemName);
            
            remainingCooldown = reference.ability.cooldown;
            disabled = true;


            CombatManager combatManager = GameManager.instance.combatManager;
            combatManager.currentEntity.DisablePart(this);

            

            CombatAbility combatAbility = (CombatAbility)reference.ability;
            Debug.Log(combatAbility.target.ToString());
            Debug.Log(combatAbility.statModifiers.Count);
            if (combatAbility.target == CombatTarget.Enemy)
            {
                CombatEntity other = combatManager.enemy;
                if (combatManager.currentEntity != combatManager.player)
                {
                    other = combatManager.player;
                }

                foreach (StatModifier statModifier in combatAbility.statModifiers)
                {
                    if (statModifier.statName == "CurrHP")
                    {
                        Debug.Log("Damaging other");
                        int damage = statModifier.value + combatManager.currentEntity.combatStats.GetStat("Dmg");
                        if (UnityEngine.Random.Range(0, 100) < combatManager.currentEntity.combatStats.GetStat("CritChance"))
                        {
                            Debug.Log("Crit!");
                            damage += combatManager.currentEntity.combatStats.GetStat("CritDmg");
                        }
                        Debug.Log("Damaging value: " + damage);
                        other.Damage(damage);
                        combatManager.currentEntity.Damage(other.combatStats.GetStat("Thorns"));
                    }
                    else
                    {
                        other.combatStats.ModifyStat(statModifier);
                    }
                }
            }
            else
            {
                foreach (StatModifier statModifier in combatAbility.statModifiers)
                {
                    if (statModifier.statName == "CurrHP")
                    {
                        combatManager.currentEntity.combatStats.ModifyStat("CurrHP", combatManager.currentEntity.combatStats.GetStat("Restoration") + statModifier.value);
                    }
                    else
                    {
                        combatManager.currentEntity.combatStats.ModifyStat(statModifier);
                    }
                }
            }

            return true;
        }
        return false;
    }

    public Part(Item item)
    {
        reference = item;
    }
}
