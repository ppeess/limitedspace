﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyPreset", menuName = "Enemy/Enemy Preset")]
public class EnemyPreset : ScriptableObject
{
    public Stats baseStats;
    public List<ItemInstance> availableItems;
    public int difficulty = 1;
}
