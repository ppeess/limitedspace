﻿using System.Collections;
using UnityEngine;


public class CombatPartView : MonoBehaviour
{
    public CombatAbilityView combatAbilityView;
    public CombatSlot combatSlot;
    public Part part;

    public void Init(Part part)
    {
        this.part = part;
        combatAbilityView.Init(part);
        combatSlot.Init(part);
    }

    public void Update()
    {
        if (part.reference.ability == null)
        {
            combatAbilityView.gameObject.SetActive(false);
        }
        else
        {
            combatAbilityView.gameObject.SetActive(true);
        }
    }
}

public enum ViewMode
{
    Player, 
    Enemy
}
