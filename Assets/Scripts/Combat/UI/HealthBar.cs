﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image filling;
    public Image white;
    public CombatEntity combatEntity;
    public int maxOffset = -145;
    public float offset = 0;
    public float lerpTime;

    public int currHP;
    public int maxHP;

    private bool initialised = false;

    public void Init(CombatEntity combatEntity)
    {
        initialised = true;
        this.combatEntity = combatEntity;
        maxHP = combatEntity.combatStats.GetStat("MaxHP");
    }

    public void Reset()
    {
        initialised = false;
        combatEntity = null;
    }

    private void Update()
    {
        if (initialised)
        {
            SetFillAmount();
        }
    }

    public void SetFillAmount()
    {
        if (currHP != combatEntity.combatStats.GetStat("CurrHP"))
        {
            currHP = combatEntity.combatStats.GetStat("CurrHP");
            float oldOffset = offset;
            offset = ((1f - (float)(currHP) / (float)(maxHP)) * (float)maxOffset);

            filling.GetComponent<RectTransform>().localPosition = new Vector3(offset, 0, 0);
            white.GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Lerp(white.GetComponent<RectTransform>().localPosition.x, offset, Time.deltaTime * lerpTime), 0, 0);
        }
        white.GetComponent<RectTransform>().localPosition = new Vector3(Mathf.Lerp(white.GetComponent<RectTransform>().localPosition.x, offset, Time.deltaTime * lerpTime), 0, 0);
    }
}
