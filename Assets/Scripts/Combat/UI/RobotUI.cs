﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RobotUI : MonoBehaviour
{
    public Image periphery;
    public Image head;
    public Image right;
    public Image left;
    public Image casing;

    public Part peripheryItem;
    public Part headItem;
    public Part rightItem;
    public Part leftItem;
    public Part casingItem;

    public void Init(Part periphery, Part head, Part right, Part left, Part casing)
    {
        this.peripheryItem = periphery;
        this.headItem = head;
        this.rightItem = right;
        this.leftItem = left;
        this.casingItem = casing;

        if (periphery != null && periphery.reference != null)
        {
            this.periphery.sprite = periphery.reference.icon;
        }
        if (head != null && head.reference != null)
        {
            this.head.sprite = head.reference.icon;
        }
        if (right != null && right.reference != null)
        {
            this.right.sprite = right.reference.icon;
        }
        if (left != null && left.reference != null)
        {
            this.left.sprite = left.reference.icon;
        }
        if (casing != null && casing.reference != null)
        {
            this.casing.sprite = casing.reference.icon;
        }   
    }
}
