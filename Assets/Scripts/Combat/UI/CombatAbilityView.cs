﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CombatAbilityView : MonoBehaviour
{
    public Part part;
    public GameObject disabledIndicator;
    public TMP_Text name;
    public TMP_Text cooldownValue;
    public GameObject abilityTooltip;
    public Button button;

    public void Init(Part part)
    {
        this.part = part;
        
        if (part != null)
        {
            if (part.reference != null && part.reference.ability != null)
            {
                cooldownValue.text = part.reference.ability.cooldown.ToString();
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(delegate
                {
                    part.Use();
                });
            }
            else
            {
                abilityTooltip.SetActive(false);
            }
        }
        else
        {
            abilityTooltip.SetActive(false);
        }
    }

    private void Update()
    {
        if (part != null)
        {
            if (part.reference != null && part.reference.ability != null)
            {
                if (part.disabled)
                {
                    disabledIndicator.SetActive(true);
                }
                else
                {
                    disabledIndicator.SetActive(false);
                }
            }
            else
            {
                disabledIndicator.SetActive(false);
            }
        }
        else
        {
            disabledIndicator.SetActive(false);
        }
    }
}
