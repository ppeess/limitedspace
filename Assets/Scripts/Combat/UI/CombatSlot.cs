﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CombatSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Part part;
    public GameObject disabledIndicator;

    private Image image;
    public Vector3 tooltipOffset = new Vector3(0, 3f, 0);
    private ItemTooltip currentTooltip;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public void Init(Part part)
    {
        image = GetComponent<Image>();
        if (part != null && part.reference != null)
        {
            this.part = part;
            image.sprite = part.reference.icon;
        }
    }

    private void Update()
    {
        if (part != null)
        {
            if (part.disabled)
            {
                disabledIndicator.SetActive(true);
            }
            else
            {
                disabledIndicator.SetActive(false);
            }
        }
        else
        {
            disabledIndicator.SetActive(false);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        currentTooltip = GameManager.instance.uiManager.GetTooltip(part.reference);
        RectTransform currentRectTransform = currentTooltip.GetComponent<RectTransform>();
        currentRectTransform.SetParent(transform.root);
        currentRectTransform.localScale = Vector3.one;
        currentRectTransform.position = GetComponent<RectTransform>().position + tooltipOffset;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }

    private void OnDisable()
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }

    private void OnDestroy()
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }
}