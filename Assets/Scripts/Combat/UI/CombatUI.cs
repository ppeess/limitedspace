﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CombatUI : MonoBehaviour
{
    public HealthBar playerHealthBar;
    public HealthBar enemyHealthBar;

    public GameObject abilityHolder;

    public CombatPartView peripheryView;
    public CombatPartView headView;
    public CombatPartView casingView;
    public CombatPartView rightView;
    public CombatPartView leftView;

    public CombatSlot enemyPeripheryView;
    public CombatSlot enemyHeadView;
    public CombatSlot enemyCasingView;
    public CombatSlot enemyRightView;
    public CombatSlot enemyLeftView;

    public RobotUI playerRobot;
    public RobotUI enemyRobot;

    public void UpdateUI()
    {
        CombatEntity player = GameManager.instance.combatManager.player;
        CombatEntity enemy = GameManager.instance.combatManager.enemy;
        
        peripheryView.Init(player.periphery);
        headView.Init(player.cpu);
        casingView.Init(player.casing);
        rightView.Init(player.right);
        leftView.Init(player.left);

        enemyPeripheryView.Init(enemy.periphery);
        enemyHeadView.Init(enemy.cpu);
        enemyCasingView.Init(enemy.casing);
        enemyRightView.Init(enemy.right);
        enemyLeftView.Init(enemy.left);
    }
}
