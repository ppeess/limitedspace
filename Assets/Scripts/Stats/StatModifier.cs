﻿using System.Collections;
using UnityEngine;

[System.Serializable]
public class StatModifier
{
    public string statName;
    public int value;
}
