using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusEffects : MonoBehaviour
{
    [SerializeField]
    private List<StatusEffect> effects;

    private void Start()
    {
        effects = new List<StatusEffect>();
        foreach(StatusEffectType effectType in Enum.GetValues(typeof(StatusEffectType)))
        {
            effects.Add(new StatusEffect(effectType));
        }        
    }

    public void UpdateStatus(StatusEffectType type, int value)
    {
        StatusEffect statusEffect = GetStatus(type);
        if (statusEffect != null)
        {
            statusEffect.ModifySourceCount(value);
        }
    }

    public void UpdateImmunity(StatusEffectType type, int value)
    {
        StatusEffect statusEffect = GetStatus(type);
        if (statusEffect != null)
        {
            statusEffect.ModifyImmuneCount(value);
        }
    }

    public StatusEffect GetStatus(StatusEffectType type)
    {
        foreach(StatusEffect statusEffect in effects)
        {
            if (statusEffect.type == type)
            {
                return statusEffect;
            }
        }

        Debug.LogError("Status effect not found for type" + type.ToString());
        return null;
    }
}

public enum StatusEffectType
{
    Stun,
    Overcharge,
    Interference,
    Overheat,
    Glitch,
    Hack
}

[Serializable]
public class StatusEffect
{
    public StatusEffectType type;
    public bool value = false;
    public bool immune = false;

    public int immuneSourceCount = 0;
    public int statusSourceCount = 0;

    public StatusEffect(StatusEffectType type)
    {
        this.type = type;
    }

    public void ModifySourceCount(int value)
    {
        statusSourceCount += value;
        if (statusSourceCount > 0 && !immune)
        {
            this.value = true;
        }
        else
        {
            this.value = false;
        }
    }

    public void ModifyImmuneCount(int value)
    {
        immuneSourceCount += value;
        if (immuneSourceCount > 0)
        {
            this.immune = true;
        }
        else
        {
            this.immune = false;
        }
    }
}