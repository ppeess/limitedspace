using Assets.Scripts.Stats;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Stats
{
    public StatPreset statPreset;
    public List<Stat> stats = new List<Stat>();

    public void Init()
    {
        stats = new List<Stat>();
        foreach (Stat stat in statPreset.stats)
        {
            stats.Add(new Stat(stat));
        }
    }

    public void ModifyStat(string statName, int value, int multiplier = 1)
    {
        Stat stat = FindStat(statName);
        if (stat != null)
        {
            stat.baseValue += value * multiplier;
        }
    }

    public void ModifyStat(StatModifier modifier, int multiplier = 1)
    {
        ModifyStat(modifier.statName, modifier.value, multiplier);
    }

    public int GetStat(string statName)
    {
        Stat stat = FindStat(statName);
        if (stat == null)
        {
            Debug.LogError("Stat " + statName + " not found.");
            return 0;
        }
        else
        {
            return stat.baseValue;
        }
    }

    private Stat FindStat(string statName)
    {
        foreach (Stat stat in stats)
        {
            if (stat.statName == statName)
            {
                return stat;
            }
        }
        Debug.LogError("Stat " + statName + " not found.");
        return null;
    }
}