﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Stats
{

    [CreateAssetMenu(fileName = "Stat Preset", menuName = "Stats/Stat Preset")]
    public class StatPreset : ScriptableObject
    {
        public List<Stat> stats;
    }
}