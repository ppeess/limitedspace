using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    public string statName;
    public int baseValue;

    public Stat (Stat stat)
    {
        this.statName = stat.statName;
        this.baseValue = stat.baseValue;
    }

    public Stat (string statName, int baseValue)
    {
        this.statName = statName;
        this.baseValue = baseValue;
    }
}
