﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopUI : MonoBehaviour
{
    public ItemInstance itemInstance;
    public List<ShopItemDisplay> itemDisplays = new List<ShopItemDisplay>();
    public RectTransform content;
    public ItemTooltip itemTooltip;


    public GameObject selectedView;
    public ShopData shopData;
    public Inventory inventory;

    // Use this for initialization
    void Start()
    {
        shopData = GameManager.instance.shopData;
        inventory = GameManager.instance.inventory;
    }

    private void OnEnable()
    {
        shopData = GameManager.instance.shopData;
        inventory = GameManager.instance.inventory;
        ClearSelection();
        UpdateUI();
    }

    public void Buy()
    {
        if (itemInstance != null && inventory.scrap >= itemInstance.item.salvageValue * 3)
        {
            inventory.ModifyCount(itemInstance.item, 1);
            inventory.scrap -= itemInstance.item.salvageValue * 3;

            ShopItemDisplay toRemove = null;
            foreach (ShopItemDisplay itemDisplay in itemDisplays)
            {
                if (itemDisplay.item.item.itemName == itemInstance.item.itemName)
                {
                    Debug.Log("Removing " +  itemInstance.item.itemName);
                    toRemove = itemDisplay;
                    break;
                }
            }

            if (toRemove != null)
            {
                itemDisplays.Remove(toRemove);
                Destroy(toRemove.gameObject);
                shopData.RemoveItem(itemInstance);
            }

            ClearSelection();
        }
    }

    public void Select(ItemInstance itemInstance)
    {
        this.itemInstance = itemInstance;
        selectedView.SetActive(true);
        itemTooltip.Init(itemInstance.item);
    }

    public void RerollSelection()
    {
        shopData.Reroll();
        UpdateUI();
    }

    public void ClearSelection()
    {
        itemInstance = null;
        selectedView.SetActive(false);
    }

    public void UpdateUI()
    {
        foreach (ShopItemDisplay itemDisplay in itemDisplays)
        {
            Destroy(itemDisplay.gameObject);
        }
        foreach (RectTransform child in content)
        {
            Destroy(child.gameObject);
        }
        itemDisplays.Clear();

        foreach (ItemInstance itemInstance in shopData.items)
        {
            ShopItemDisplay itemDisplay = Instantiate(GameManager.instance.uiManager.shopItemDisplay);
            itemDisplay.Init(itemInstance);
            itemDisplay.GetComponent<RectTransform>().parent = content;
            itemDisplay.GetComponent<RectTransform>().localScale = Vector3.one;
            itemDisplays.Add(itemDisplay);            
        }
    }
}
