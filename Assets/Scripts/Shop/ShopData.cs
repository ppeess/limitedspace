﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class ShopData
{
    public List<ItemInstance> items = new List<ItemInstance>();
    public List<Item> rollableItems = new List<Item>();
    public List<Item> unownedItems = new List<Item>();
    public List<Item> initialItems = new List<Item>();
    public int itemCount = 8;
    public List<RarityWeight> rarityWeights;
    List<Item> rollList = new List<Item>();

    private Dictionary<Rarity, int> rarityDict = new Dictionary<Rarity, int>();

    public void Init()
    {
        foreach (Item item in initialItems)
        {
            items.Add(new ItemInstance(item, 1));
        }
    }

    public void RemoveItem(ItemInstance item)
    {
        items.Remove(item);
    }

    public void Reroll()
    {
        int playerRating = GameManager.instance.playerManager.playerRating;
        rarityDict.Clear();
        rollList.Clear();
        items.Clear();

        foreach (RarityWeight rarityWeight in rarityWeights)
        {
            rarityDict.Add(rarityWeight.rarity, rarityWeight.weight);
        }

        unownedItems.Clear();
        foreach (Item item in rollableItems) 
        {
            if (GameManager.instance.inventory.FindItem(item) == null) 
            {
                unownedItems.Add(item);
            }
        }

        foreach (Item item in unownedItems)
        {
            for (int i = 0; i < rarityDict[item.rarity]; i++)
            {
                rollList.Add(item);
            }
        }

        for (int i = 0; i < itemCount; i++)
        {
            if (rollList.Count > 0)
            {
                items.Add(new ItemInstance(RollItem(), 1));
            }
        }
    }

    public Item RollItem()
    {
        int index = UnityEngine.Random.Range(0, rollList.Count);
        Item item = rollList[index];
        rollList.RemoveAll(entry => item == entry);
        return item;
    }
}

[Serializable]
public class RarityWeight
{
    public Rarity rarity;
    public int weight;
}