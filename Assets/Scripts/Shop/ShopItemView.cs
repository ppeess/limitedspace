﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShopItemView : MonoBehaviour
{
    public RectTransform content;

    private ShopData shopData;
    private List<ShopItemDisplay> itemDisplays = new List<ShopItemDisplay>();

    private void Awake()
    {
        shopData = GameManager.instance.shopData;
    }

    public void Clear()
    {
        foreach (ShopItemDisplay itemDisplay in itemDisplays)
        {
            Destroy(itemDisplay.gameObject);
        }
        itemDisplays = new List<ShopItemDisplay>();
    }

    public void Init()
    {
        Clear();
        foreach(ItemInstance itemInstance in shopData.items)
        {
            if (itemInstance.item.baseType == BaseType.Part && itemInstance.item.partType != PartType.Core)
            {
                ShopItemDisplay itemDisplay = Instantiate(GameManager.instance.uiManager.shopItemDisplay);
                itemDisplay.Init(itemInstance);
                itemDisplay.GetComponent<RectTransform>().parent = content;
                itemDisplay.GetComponent<RectTransform>().localScale = Vector3.one;
                itemDisplays.Add(itemDisplay);
            }
        }
    }

    void OnEnable()
    {
        Clear();
        Init();
    }
}
