﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopItemDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public ItemInstance item;

    private Image image;
    private Vector3 tooltipOffset;
    private ItemTooltip currentTooltip;
    private Button button;

    private void Awake()
    {
        tooltipOffset = new Vector3(GetComponent<RectTransform>().rect.width/16 + 2.5f, 0, 0);
        image = GetComponent<Image>();
        button = GetComponent<Button>();
    }

    public void Init(ItemInstance itemInstance)
    {
        item = itemInstance;
        image.sprite = itemInstance.item.icon;
        button.onClick.AddListener(
            delegate { 
                GameManager.instance.uiManager.shopUI.Select(item); 
            }
        );
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        currentTooltip = GameManager.instance.uiManager.GetTooltip(item);
        RectTransform currentRectTransform = currentTooltip.GetComponent<RectTransform>();
        currentRectTransform.SetParent(transform.root);
        currentRectTransform.localScale = Vector3.one;
        currentRectTransform.position = GetComponent<RectTransform>().position + tooltipOffset;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }

    private void OnDisable()
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }

    private void OnDestroy()
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }
}
