﻿public enum PartType
{
    CPU,
    Core,
    Periphery,
    Casing,
    Left,
    Right,
    Any,
    All
}