using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Items/Item")]
public class Item : ScriptableObject
{
    public string itemName;
    public string description;
    public Rarity rarity;
    public BaseType baseType;
    public PartType partType;
    public Sprite icon;
    public List<StatModifier> statModifiers = new List<StatModifier>();
    public Ability ability;
    public bool stackable;
    public int salvageValue;
    public int coreNumber;
}
