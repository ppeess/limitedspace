using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerManager
{
    public Stats playerStats;
    public Stats combatStats;
    public int playerRating;
    public void Init()
    {
        playerStats.Init();
        // TODO: Handle combat stats
    }
}
