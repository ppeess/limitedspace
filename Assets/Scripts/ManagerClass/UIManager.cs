using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("Core Equipment System")]
    public Sprite cpuSlotDisabled;
    public Sprite cpuSlotEnabled;
    public EquipmentManager equipmentManager;
    public SelectionUI selectionUI;
    public ItemDisplay itemDisplay;

    [Header("Slot Machine")]
    public GameObject slotMachine;

    [Header("Item Tooltip")]
    public ItemTooltip itemTooltip;
    public StatDisplay statDisplay;
    public StatModifierDisplay statModifierDisplay;

    [Header("Inventory")]
    public InventoryUI inventoryUI;
    public ItemDisplayInventory itemDisplayInventory;

    [Header("Inventory")]
    public ShopUI shopUI;
    public ShopItemDisplay shopItemDisplay;

    [Header("Combat")]
    public CombatUI combatUI;
    public WinUI winUI;
    public GameObject loseUI;

    public Color positiveColor = Color.green;
    public Color negativeColor = Color.red;

    public List<IconAssociation> iconAssociations = new List<IconAssociation>();

    public ItemTooltip GetTooltip(ItemInstance item)
    {
        return GetTooltip(item.item);
    }

    public ItemTooltip GetTooltip(Item item)
    {
        ItemTooltip tooltipInstance = Instantiate(itemTooltip);
        tooltipInstance.Init(item);

        return tooltipInstance;
    }

    public Sprite GetIcon(string key)
    {
        foreach (IconAssociation iconAssociation in iconAssociations) 
        { 
            if (iconAssociation.name == key)
            {
                return iconAssociation.sprite;
            }
        }

        Debug.LogError("Invalid icon: " + key);
        return null;
    }
}

[Serializable]
public class IconAssociation
{
    public string name;
    public Sprite sprite;
}