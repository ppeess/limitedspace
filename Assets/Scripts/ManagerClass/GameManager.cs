using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public PlayerManager playerManager;
    public UIManager uiManager;
    public CombatManager combatManager;
    public ShopData shopData;
    public Inventory inventory = new Inventory();

    public List<EnemyPreset> enemyPreset;

    public static GameManager instance;

    private void Awake()
    {
        instance = this;
        playerManager.Init();
        shopData.Init();
        inventory.Init(playerManager.playerStats);
    }

    public void StartCombat()
    {
        if (playerManager.playerRating < 3)
        {
            combatManager.Init(playerManager, inventory, enemyPreset[0]);
        }
        else if (playerManager.playerRating < 7) 
        {
            combatManager.Init(playerManager, inventory, enemyPreset[1]);
        }
        else
        {
            combatManager.Init(playerManager, inventory, enemyPreset[2]);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }
}