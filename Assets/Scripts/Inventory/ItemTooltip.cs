﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemTooltip : MonoBehaviour
{
    public TMP_Text itemName;
    public TMP_Text itemDescription;
    public TMP_Text itemType;
    public RectTransform statHolder;
    public AbilityDisplay abilityDisplay;
    private StatModifierDisplay statModifierDisplay;

    private void Awake()
    {
        statModifierDisplay = GameManager.instance.uiManager.statModifierDisplay;
    }

    public void Init(Item item)
    {
        itemName.text = item.name;
        itemDescription.text = item.description;
        if (item.baseType == BaseType.CoreUpgrade)
        {
            itemType.text = "Core Upgrade";
        }
        else
        {
            itemType.text = item.partType.ToString();
            if (item.partType == PartType.Core)
            {
                itemType.text += " - " + item.coreNumber + "-Core";
            }
        }

        foreach (Transform child in statHolder)
        {
            Destroy(child.gameObject);
        }
        
        foreach (StatModifier modifier in item.statModifiers)
        {
            StatModifierDisplay displayInstance = Instantiate(statModifierDisplay);
            displayInstance.Init(modifier);
            displayInstance.transform.parent = statHolder;
            displayInstance.transform.localScale = Vector3.one;
        }

        if (item.ability != null)
        {
            abilityDisplay.Init(item.ability);
            abilityDisplay.gameObject.SetActive(true);
        }
        else
        {
            abilityDisplay.gameObject.SetActive(false);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(this.GetComponent<RectTransform>());
    }
}
