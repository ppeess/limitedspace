﻿using Assets.Scripts.Abilities;
using System.Collections;
using TMPro;
using UnityEngine;

// TODO: REMOVE THIS
[ExecuteInEditMode]
public class AbilityDisplay : MonoBehaviour
{
    public RectTransform abilityDisplay;
    public TMP_Text abilityName;
    public TMP_Text description;
    public TMP_Text cooldown;
    public TMP_Text charges;
    public GameObject chargeHolder;
    public GameObject playerAbilityIndicator;    
    public GameObject combatAbilityIndicator;

    public void Init(Ability ability)
    {
        abilityName.text = ability.name;
        description.text = ability.description;
        cooldown.text = ability.cooldown.ToString();

        if (ability.abilityType == AbilityType.Player)
        {
            combatAbilityIndicator.SetActive(false);
            int chargeValue = ((PlayerAbility)ability).charges;
            if (chargeValue > 1)
            {
                charges.text = chargeValue.ToString();
            }
            else
            {
                chargeHolder.SetActive(false);
            }
        }
        else
        {
            playerAbilityIndicator.SetActive(false);
            chargeHolder.SetActive(false);
        }
    }

    // TODO: REMOVE THIS
    private void Update()
    {
        UpdateSize();
    }

    public void UpdateSize()
    {
        Rect previousRect = GetComponent<RectTransform>().rect;
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, abilityDisplay.GetComponent<RectTransform>().rect.height + 2);
    }
}
