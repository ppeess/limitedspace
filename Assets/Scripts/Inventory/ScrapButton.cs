using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrapButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public TMP_Text textTarget;
    private InventoryUI inventoryUI;
    private ShopUI shopUI;
    public int modifier = 1;
    public ScrapValueType scrapValueType = ScrapValueType.Scrap;

    private void Start()
    {
        inventoryUI = GameManager.instance.uiManager.inventoryUI;
        shopUI = GameManager.instance.uiManager.shopUI;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        textTarget.gameObject.SetActive(true);
        
        if (scrapValueType == ScrapValueType.Scrap)
        {
            if (inventoryUI.itemInstance != null)
            {
                int value = inventoryUI.itemInstance.count * inventoryUI.itemInstance.item.salvageValue * modifier;
                if (scrapValueType == ScrapValueType.Buy)
                {
                    value = value * 3;
                }
                SetValue(value);
            }
        }
        else
        {
            if (shopUI.itemInstance != null)
            {
                int value = shopUI.itemInstance.count * shopUI.itemInstance.item.salvageValue * modifier;
                if (scrapValueType == ScrapValueType.Buy)
                {
                    value = value * 3;
                }
                SetValue(value);
            }
        }
    }

    public void OnDisable()
    {
        textTarget.gameObject.SetActive(false);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        textTarget.gameObject.SetActive(false);
    }

    public void SetValue(int value)
    {
        if (value > 0)
        {
            textTarget.text = "+" + value.ToString();   
        }
        else
        {
            textTarget.text = value.ToString();
        }
    }
}

public enum ScrapValueType
{
    Scrap, 
    Buy
}