﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatOverview : MonoBehaviour
{
    public Stats statSource;
    public RectTransform statHolder;
    public StatMode statMode;
    private StatDisplay statDisplay;
    private Dictionary<string, int> stats = new Dictionary<string, int>();
    private Dictionary<string, StatDisplay> displays = new Dictionary<string, StatDisplay>();
    
    void Awake()
    {
        if(statMode == StatMode.Base)
        {
            statSource = GameManager.instance.playerManager.playerStats;
        }
        else
        {
            statSource = GameManager.instance.playerManager.combatStats;
        }
        stats = new Dictionary<string, int>();
        statDisplay = GameManager.instance.uiManager.statDisplay;
        
        foreach(Stat stat in statSource.stats)
        {
            stats.Add(stat.statName, stat.baseValue);
            StatDisplay currDisplay = Instantiate(statDisplay);
            currDisplay.GetComponent<RectTransform>().parent = statHolder;
            currDisplay.GetComponent<RectTransform>().localScale = Vector3.one;
            currDisplay.Init(stat);
            displays.Add(stat.statName, currDisplay);
        }
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Stat stat in statSource.stats)
        {
            if (stats[stat.statName] != stat.baseValue)
            {
                // Stat changed, update values
                stats[stat.statName] = stat.baseValue;
                displays[stat.statName].value.text = stat.baseValue.ToString();
            }
        }
    }
}

public enum StatMode
{
    Base, 
    Combat
}