﻿using System.Collections;
using TMPro;
using UnityEngine;


public class RatingDisplay : MonoBehaviour
{
    public PlayerManager playerManager;
    public TMP_Text text;

    void Start()
    {
        playerManager = GameManager.instance.playerManager;
    }

    void Update()
    {
        text.text = playerManager.playerRating.ToString();
    }
}
