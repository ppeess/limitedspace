﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatModifierDisplay : MonoBehaviour
{
    public Image icon;
    public TMP_Text value;

    public void Init(StatModifier modifier)
    {
        Sprite sprite = GameManager.instance.uiManager.GetIcon(modifier.statName);
        if (sprite != null ) 
        {
            icon.sprite = sprite;
        }

        if (modifier.value > 0)
        {
            value.color = GameManager.instance.uiManager.positiveColor;
            value.text = "+" + modifier.value.ToString();
        }
        else
        {
            value.color = GameManager.instance.uiManager.negativeColor;
            value.text = modifier.value.ToString();
        }
    }
}
