using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class EquipmentManager : MonoBehaviour
{
    public List<EquipmentSlot> slots;
    public EquipmentSlot coreSlot;
    public int activeSlots = 0;
    public EquipmentSlot lastInteraction;

    private void OnEnable()
    {
        SetCoresActive();
    }

    public void SetCoresActive()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            if (i < activeSlots)
            {
                slots[i].Enable();
            }
            else
            {
                slots[i].Disable();
            }
        }
    }

    public void UpdateCore(Item item)
    {
        if (item == null)
        {
            activeSlots = 0;
        }
        else
        {
            activeSlots = item.coreNumber;
        }

        SetCoresActive();
    }
}
