using System;

[Serializable]
public class ItemInstance
{
    public Item item;
    public int count;
    public bool equipped = false;

    public ItemInstance(Item item, int count)
    {
        this.item = item;
        this.count = count;
    }
}
