﻿using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    private Inventory inventory;
    public ItemInstance itemInstance;

    public ItemTooltip itemTooltip;
    public ItemView itemView;
    public GameObject selectedView;

    void Awake()
    {
        inventory = GameManager.instance.inventory;
    }

    private void OnEnable()
    {
        Deselect();
    }
    
    public void Select(ItemInstance itemInstance)
    {
        this.itemInstance = itemInstance; 
        selectedView.SetActive(true);
        itemTooltip.Init(itemInstance.item);
    }
    
    public void Salvage()
    {
        if (itemInstance != null)
        {
            inventory.Salvage(itemInstance);
            Deselect();
            itemView.Init();
        }
    }

    public void Deselect()
    {
        itemInstance = null;
        selectedView.SetActive(false);
    }
}
