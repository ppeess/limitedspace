﻿using System.Collections;
using TMPro;
using UnityEngine;


public class ScrapDisplay : MonoBehaviour
{
    public Inventory inventory;
    public TMP_Text text;

    void Start()
    {
        inventory = GameManager.instance.inventory;
    }

    void Update()
    {
        text.text = inventory.scrap.ToString();
    }
}
