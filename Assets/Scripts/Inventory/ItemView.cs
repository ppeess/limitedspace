﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemView : MonoBehaviour
{
    public RectTransform content;

    private Inventory inventory;
    private List<ItemDisplayInventory> itemDisplays = new List<ItemDisplayInventory>();

    private void Awake()
    {
        inventory = GameManager.instance.inventory;
    }

    public void Clear()
    {
        foreach (ItemDisplayInventory itemDisplay in itemDisplays)
        {
            Destroy(itemDisplay.gameObject);
        }
        itemDisplays = new List<ItemDisplayInventory>();
    }

    public void Init()
    {
        Clear();
        foreach(ItemInstance itemInstance in inventory.items)
        {
            if (itemInstance.item.baseType == BaseType.Part && itemInstance.item.partType != PartType.Core)
            {
                ItemDisplayInventory itemDisplay = Instantiate(GameManager.instance.uiManager.itemDisplayInventory);
                itemDisplay.Init(itemInstance);
                itemDisplay.GetComponent<RectTransform>().parent = content;
                itemDisplay.GetComponent<RectTransform>().localScale = Vector3.one;
                itemDisplays.Add(itemDisplay);
            }
        }
    }

    void OnEnable()
    {
        Clear();
        Init();
    }
}
