﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDisplay : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Item item;
    public GameObject equippedIndicator;

    private Image image;
    private Vector3 tooltipOffset;
    private ItemTooltip currentTooltip;
    private bool equipped = false;
    private SelectionUI selectionUI;
    private Button button;

    private void Awake()
    {
        tooltipOffset = new Vector3(GetComponent<RectTransform>().rect.width/16 + 2.5f, 0, 0);
        image = GetComponent<Image>();
        button = GetComponent<Button>();
    }

    public void Init(ItemInstance itemInstance, SelectionUI selectionUI)
    {
        equipped = itemInstance.equipped;
        item = itemInstance.item;
        image.sprite = itemInstance.item.icon;
        this.selectionUI = selectionUI;

        if(equipped)
        {
            equippedIndicator.SetActive(true);
        }
        else
        {
            equippedIndicator.SetActive(false);
        }

        button.onClick.AddListener(delegate { selectionUI.EquipItem(itemInstance); });
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        currentTooltip = GameManager.instance.uiManager.GetTooltip(item);
        RectTransform currentRectTransform = currentTooltip.GetComponent<RectTransform>();
        currentRectTransform.SetParent(transform.root);
        currentRectTransform.localScale = Vector3.one;
        currentRectTransform.position = GetComponent<RectTransform>().position + tooltipOffset;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }

    private void OnDisable()
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }

    private void OnDestroy()
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }
}
