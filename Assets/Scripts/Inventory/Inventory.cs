using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory
{
    public int scrap = 25;
    public List<ItemInstance> items = new List<ItemInstance>();
    private Stats playerStats;

    public void Init(Stats playerStats)
    {
        this.playerStats = playerStats;
    }

    public void ModifyCount(Item item, int count)
    {
        if (item.stackable)
        {
            ItemInstance alreadyOwned = FindItem(item);
            if (alreadyOwned != null)
            {
                alreadyOwned.count += count;
                if (alreadyOwned.count <= 0)
                {
                    items.Remove(alreadyOwned);
                }
            }
            else
            {
                ItemInstance addedItems = new ItemInstance(item, count);
                items.Add(addedItems);
                CreatePopup(addedItems);
            }
        }
        else
        {
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    ItemInstance addedItems = new ItemInstance(item, 1);
                    items.Add(addedItems);
                    CreatePopup(addedItems);
                }
            }
            else
            {
                for (int i = 0; i < Mathf.Abs(count); i++)
                {
                    ItemInstance alreadyOwned = FindItem(item);
                    if (alreadyOwned != null)
                    {
                        items.Remove(alreadyOwned);
                    }
                }
            }
        }
    }

    public List<ItemInstance> GetItemsForType(BaseType baseType)
    {
        List<ItemInstance> foundItems = new List<ItemInstance>();
        foreach (ItemInstance item in items) { 
            if (item.item.baseType == baseType)
            {
                foundItems.Add(item);
            }
        }
        return foundItems;
    }

    public List<ItemInstance> GetItemsForType(PartType partType)
    {
        List<ItemInstance> foundItems = new List<ItemInstance>();
        foreach (ItemInstance item in items)
        {
            if (item.item.partType == partType)
            {
                foundItems.Add(item);
            }
        }
        return foundItems;
    }

    public void Salvage(ItemInstance item, int count = 1)
    {
        scrap += item.item.salvageValue * count;
        ModifyCount(item.item, -1 * count);
    }

    public ItemInstance FindItem(Item item)
    {
        return FindItem(item.itemName);
    }

    public ItemInstance FindItem(string itemName)
    {
        foreach(ItemInstance itemInstance in items)
        {
            if (itemInstance.item.itemName == itemName)
            {
                return itemInstance;
            }
        }

        Debug.Log("Item not found: " + itemName);
        return null;
    }

    public void Equip(ItemInstance item, bool reequipped = false)
    {
        if (!item.equipped || reequipped) {
            foreach (StatModifier modifier in item.item.statModifiers)
            {
                playerStats.ModifyStat(modifier);
            }
            item.equipped = true;
        }
        else
        {
            List<EquipmentSlot> slots = GameManager.instance.uiManager.equipmentManager.slots;

            foreach (EquipmentSlot slot in slots)
            {
                if (slot.containedItem == item && slot != GameManager.instance.uiManager.equipmentManager.lastInteraction)
                {
                    if (!slot.slotEnabled)
                    {
                        foreach (StatModifier modifier in item.item.statModifiers)
                        {
                            playerStats.ModifyStat(modifier);
                        }
                    }
                    slot.Clear();
                }
            }
        }

        if (item.item.baseType == BaseType.Part && item.item.partType == PartType.Core)
        {
            GameManager.instance.uiManager.equipmentManager.UpdateCore(item.item);
        }
    }

    public void Unequip(ItemInstance item, bool keep = false)
    {
        if (item.equipped)
        {
            foreach (StatModifier modifier in item.item.statModifiers)
            {
                playerStats.ModifyStat(modifier, -1);
            }
            item.equipped = keep;

            if (item.item.baseType == BaseType.Part && item.item.partType == PartType.Core)
            {
                GameManager.instance.uiManager.equipmentManager.UpdateCore(null);
            }
        }
    }


    public void CreatePopup(ItemInstance itemInstance)
    {
        Debug.LogError("Inventory: CreatePopup not implemented yet!");
    }
}
