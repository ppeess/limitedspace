﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Inventory
{
    public class ScrollBG : MonoBehaviour
    {
        public float speed = 1f; 

        // Update is called once per frame
        void Update()
        {
            GetComponent<RectTransform>().localPosition = GetComponent<RectTransform>().localPosition - new Vector3(1, 0, 0) * Time.deltaTime * speed;
            if (GetComponent<RectTransform>().localPosition.x <= -320)
            {
                GetComponent<RectTransform>().localPosition = new Vector3(320, 0);
            }
        }
    }
}