﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionUI : MonoBehaviour
{
    public EquipmentSlot slot;
    public RectTransform holder;
    public List<ItemDisplay> currentDisplays = new List<ItemDisplay>();

    public void Init(EquipmentSlot equipmentSlot, List<ItemInstance> items)
    {
        if (equipmentSlot == slot)
        {
            gameObject.SetActive(false);
            return;
        }
        
        foreach(ItemDisplay itemDisplay in currentDisplays)
        {
            Destroy(itemDisplay.gameObject);
        }
        currentDisplays = new List<ItemDisplay>{};

        this.slot = equipmentSlot;

        foreach (ItemInstance itemInstance in items)
        {
            // Create item display
            ItemDisplay display = Instantiate(GameManager.instance.uiManager.itemDisplay);
            display.GetComponent<RectTransform>().parent = holder;
            display.GetComponent<RectTransform>().localScale = Vector3.one;
            display.Init(itemInstance, this);
            currentDisplays.Add(display);
        }
    }

    public void EquipItem(ItemInstance item)
    {
        if (slot.containsItem)
        {
            GameManager.instance.inventory.Unequip(slot.containedItem);
        }
        slot.InitItem(item);
        GameManager.instance.inventory.Equip(item);
        gameObject.SetActive(false);
    }

    public void ClearSlot()
    {
        if (slot.containsItem)
        {
            GameManager.instance.inventory.Unequip(slot.containedItem);
            slot.Clear();
        }
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        this.slot = null;
    }
}
