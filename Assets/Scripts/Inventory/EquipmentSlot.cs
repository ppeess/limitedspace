﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EquipmentSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Inventory inventory;
    private Image holder;
    public BaseType baseType;
    public PartType partType;
    public Image selectedItemIcon;
    public ItemInstance containedItem;
    public bool containsItem = false;
    public Vector3 tooltipOffset;
    public bool slotEnabled = true;

    private ItemTooltip currentTooltip;

    private void Start()
    {
        inventory = GameManager.instance.inventory;
        holder = GetComponent<Image>();
    }

    /// <summary>
    /// Disables the slot, keeping the current item for comfort purposes.
    /// </summary>
    public void Disable()
    {
        holder = GetComponent<Image>();
        if (slotEnabled)
        {
            slotEnabled = false;
            holder.sprite = GameManager.instance.uiManager.cpuSlotDisabled;
            if (containsItem)
            {
                inventory.Unequip(containedItem, true);
            }
        }
    }

    /// <summary>
    /// Enables a slot and equips any previously contained items. 
    /// </summary>
    public void Enable()
    {
        holder = GetComponent<Image>();
        if (!slotEnabled)
        {
            slotEnabled = true;
            holder.sprite = GameManager.instance.uiManager.cpuSlotEnabled;
            if (containsItem)
            {
                inventory.Equip(containedItem, true);
            }
        }
    }

    /// <summary>
    /// On click, displays a selection UI for the equipment slot. 
    /// </summary>
    public void DisplaySelection()
    {
        List<ItemInstance> items = new List<ItemInstance> ();
        GameManager.instance.uiManager.equipmentManager.lastInteraction = this;
        
        if (baseType == BaseType.CoreUpgrade)
        {
             items = inventory.GetItemsForType(baseType);
        }
        else
        {
            items = inventory.GetItemsForType (partType);
        }

        DisplaySelectionInterface(items, this);
    }

    /// <summary>
    /// Initializes the selection UI.
    /// </summary>
    /// <param name="selectable">List of items that may be selected</param>
    /// <param name="slot">Current slot</param>
    public void DisplaySelectionInterface(List<ItemInstance> selectable, EquipmentSlot slot)
    {
        if (slotEnabled)
        {
            SelectionUI selectionUI = GameManager.instance.uiManager.selectionUI;
            selectionUI.gameObject.SetActive(true);
            selectionUI.Init(slot, selectable);
            selectionUI.GetComponent<RectTransform>().position = GetComponent<RectTransform>().position + tooltipOffset;
        }
    }

    public void Clear()
    {
        containsItem = false;
        containedItem = null;
        selectedItemIcon.sprite = null;
        selectedItemIcon.color = new Color(255, 255, 255, 0);
    }

    public void InitItem(ItemInstance item)
    {
        containsItem = true;
        containedItem = item;
        selectedItemIcon.sprite = item.item.icon;
        selectedItemIcon.color = new Color(255, 255, 255, 1);
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        if(containsItem) 
        {
            currentTooltip = GameManager.instance.uiManager.GetTooltip(containedItem);
            RectTransform currentRectTransform = currentTooltip.GetComponent<RectTransform>();
            currentRectTransform.SetParent(transform.root);
            currentRectTransform.localScale = Vector3.one;
            currentRectTransform.position = GetComponent<RectTransform>().position + tooltipOffset;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (currentTooltip != null)
        {
            Destroy(currentTooltip.gameObject);
        }
    }
}
