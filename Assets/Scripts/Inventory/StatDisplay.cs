﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StatDisplay : MonoBehaviour
{
    public Image icon;
    public TMP_Text value;

    public void Init(Stat stat)
    {
        Sprite sprite = GameManager.instance.uiManager.GetIcon(stat.statName);
        if (sprite != null ) 
        {
            icon.sprite = sprite;
        }
        value.text = stat.baseValue.ToString();
    }
}
