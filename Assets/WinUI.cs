using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WinUI : MonoBehaviour
{
    public TMP_Text scrap;
    public TMP_Text rating;

    public void Init(int scrapReward, int ratingReward)
    {
        scrap.text = "Scrap: +" + scrapReward;
        rating.text = "Rating: +" + ratingReward;
    }
}
